// Set'ers:

void setThreshhold(float val);

void setVnom(float V);

int setConsumption(int loadNum, float AP, float V);

int setConsumptionReactive(int loadNum, float RP, float V); //DANI

int setConsumptionHarmonic(int loadNum, float HP, float V); //DANI

void NILM_Init(float threshhold, float Vnom);

/*              Functions               */

int detectActivePowerStep(float AP, float V);

int detectReactivePowerStep(float RP, float V); //DANI

int detectHarmonicPowerStep(float HP, float V); //DANI

float voltageCompensation(float V, float P);

float inverseVoltageCompensation(float V, float Pnom);

int getLoadOn(int loadNum);

void updateLoads(float AP, float RP, float HP, float V); //DANI

// Old:

int detectLoads(float activePower, float voltage);

Boolean loadOn(int loadNum, float activePower, float voltage);

float getLoadConsumption(int loadNum);

float getPreviousAP();