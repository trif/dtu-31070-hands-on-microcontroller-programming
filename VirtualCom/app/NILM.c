#include <math.h>

/*              Constants               */

static float loadConsumption[10];               // Consumption (Active Power) of up to 10 loads. Needs to be initialized.
static float loadConsumptionReactive[10];       // DANI: Reactive Power of up to 10 loads. Needs to be initialized.
static float loadConsumptionHarmonic[10];       // DANI: Harmonic Power of up to 10 loads. Needs to be initialized.
static int loadOn[10];                          // Load off: 0, load on: 1.
static float threshhold = 10.0 ;                // Threshhold in percent 
static float Vnom = 230.0 ;                     // Nominal voltage
static float previousAP[5] = {0,0,0,0,0};       // Active power from the previous reading
static float previousRP[5] = {0,0,0,0,0};       // DANI: Reactive power from the previous reading
static float previousHP[5] = {0,0,0,0,0};       // DANI: Harmonic power from the previous reading

/*              Functions               */

void setThreshhold(float val){
  threshhold = val;
}

float getLoadConsumption(int loadNum){
  return loadConsumption[loadNum -1 ];
}

float getPreviousAP(){
  return previousAP[2];
}

void setVnom(float V){
  Vnom = V;
}

void NILM_Init(float threshhold, float Vnom){
  for (int i = 0; i < 10; i++){                 // Initialize the consumptions to high values, so they don't show as 'on'.
    loadConsumption[i] = 1000000;
    loadConsumptionReactive[i] = 1000000;   //DANI
    loadConsumptionHarmonic[i] = 1000000;   //DANI
    loadOn[i] = 0;                              // Initialize all loads to 'off'.
  }
  setThreshhold(threshhold);
  setVnom(Vnom);
}

float voltageCompensation(float V, float P){            // Asuming constant impedance
  return (float) (pow( (Vnom / V) , 2 ) )* P ;          // Returns active power at nominal voltage, APnom
}

float inverseVoltageCompensation(float V, float Pnom){             // Asuming constant impedance
  return (float) (pow( (V / Vnom) , 2 ) )* Pnom ;                  // Returns active power at current voltage, AP
}

// Set'ers:

int setConsumption(int loadNum, float AP, float V){             // Returns 1 if set is succesful and 0 if set is unsuccesful
  int set = 0;
  if (loadNum > 0 & loadNum <= 10){                             // Checks that loadNum is correct
    loadConsumption[loadNum-1] = voltageCompensation(V, AP);    // Nominal consumption at Vnom
    set = 1;
  }
  return set;
}

//DANI
int setConsumptionReactive(int loadNum, float RP, float V){             // Set for Reactive Power
  int set = 0;
  if (loadNum > 0 & loadNum <= 10){                
    loadConsumptionReactive[loadNum-1] = voltageCompensation(V, RP);    
    set = 1;
  }
  return set;
}

//DANI
int setConsumptionHarmonic(int loadNum, float HP, float V){             // Set for Harmonic Power
  int set = 0;
  if (loadNum > 0 & loadNum <= 10){                
    loadConsumptionHarmonic[loadNum-1] = voltageCompensation(V, HP);    
    set = 1;
  }
  return set;
}

int getLoadOn(int loadNum){                     // Returns 1 if load is on, 0 if load is off.
  return loadOn[loadNum-1];
}

/***********************************************************************************************/

/*
Function:       detectActivePower(float AP, float V)
Parameters:     Active power
Description:    Detects if the active power is close to the consumption of any of the loads. 
Returns the load number if the difference is within the threshhold and the load turns on. 
Returns the negative load numeber if the load turns off. Returns zero if not within threshhold.
*/ 
int detectActivePowerStep(float AP, float V){                        
  float APStep = AP - previousAP[3];                    // Step change in active power, signed.
  float minDif = 1000000000;                            // Minimum difference between active power and loadConsumption. Absolute value.
  int index = 0;                                        // Index of smallest difference
  float expectedAPStep = 0;
  float diff = 0;
  for (int i = 0; i < 10; i++){                         // Compute the minimum difference
    expectedAPStep = inverseVoltageCompensation(V, loadConsumption[i]);
    diff = fabsf(expectedAPStep - fabsf(APStep) );
    if ( diff < minDif){
      minDif = diff;
      index = i;
    } 
  }
  float threshholdAP = inverseVoltageCompensation(V, loadConsumption[index])*threshhold / 100;
  if (minDif > threshholdAP ){ index = 0; }             // Checks if the difference is outside the threshhold
  else {                                                
    if ( (fabsf(AP - previousAP[0]) < threshholdAP ) && (fabsf(previousAP[3] - previousAP[4]) < threshholdAP ) ){ index++;}        // Checks if the AP has stabalized
    else {index = 0;}                                   // Converts range from 0-9 to 1-1
  }                                       
  if (APStep < 0){index = -index;}                      // Converts to negative numbers if the load is turned off
  previousAP[4] = previousAP[3]; previousAP[3] = previousAP[2]; previousAP[2] = previousAP[1]; previousAP[1] = previousAP[0]; previousAP[0] = AP;     // Sets previousAP for next time function is called       
  return index;                                         // Returns the load number. Returns 0 if not within threshhold.
}


//DANI
//SAME FUNCTION AS BEFORE, BUT FOR REACTIVE POWER
int detectReactivePowerStep(float RP, float V){                        
  float RPStep = RP - previousRP[3];                 
  float minDif = 1000000000;                
  int index = 0;                          
  float expectedRPStep = 0;
  float diff = 0;
  for (int i = 0; i < 10; i++){                  
    expectedRPStep = inverseVoltageCompensation(V, loadConsumptionReactive[i]);
    diff = fabsf(expectedRPStep - fabsf(RPStep) );
    if ( diff < minDif){
      minDif = diff;
      index = i;
    } 
  }
  float threshholdRP = inverseVoltageCompensation(V, loadConsumptionReactive[index])*threshhold / 100;
  if (minDif > threshholdRP ){ index = 0; }     
  else {                                                
    if ( (fabsf(RP - previousRP[0]) < threshholdRP ) && (fabsf(previousRP[3] - previousRP[4]) < threshholdRP ) ){ index++;}   
    else {index = 0;}                 
  }                                       
  if (RPStep < 0){index = -index;}                    
  previousRP[4] = previousRP[3]; previousRP[3] = previousRP[2]; previousRP[2] = previousRP[1]; previousRP[1] = previousRP[0]; previousRP[0] = RP;     // Sets previousAP for next time function is called       
  return index;                                 
}


//DANI
//SAME FUNCTION AS BEFORE, BUT FOR HARMONIC POWER
int detectHarmonicPowerStep(float HP, float V){                        
  float HPStep = HP - previousHP[3];                 
  float minDif = 1000000000;                
  int index = 0;                          
  float expectedHPStep = 0;
  float diff = 0;
  for (int i = 0; i < 10; i++){                  
    expectedHPStep = inverseVoltageCompensation(V, loadConsumptionHarmonic[i]);
    diff = fabsf(expectedHPStep - fabsf(HPStep) );
    if ( diff < minDif){
      minDif = diff;
      index = i;
    } 
  }
  float threshholdHP = inverseVoltageCompensation(V, loadConsumptionHarmonic[index])*threshhold / 100;
  if (minDif > threshholdHP ){ index = 0; }     
  else {                                                
    if ( (fabsf(HP - previousHP[0]) < threshholdHP ) && (fabsf(previousHP[3] - previousHP[4]) < threshholdHP ) ){ index++;}   
    else {index = 0;}                 
  }                                       
  if (HPStep < 0){index = -index;}                    
  previousHP[4] = previousHP[3]; previousHP[3] = previousHP[2]; previousHP[2] = previousHP[1]; previousHP[1] = previousHP[0]; previousHP[0] = HP;     // Sets previousAP for next time function is called       
  return index;                                 
}






/*
Function:       updateLoads(AP, V)
1) Detect change and record AP 1) wait for AP to stabalize, call detectActivePowerStep().
--> Make previousAP global as well as AP[2]
*/

void updateLoads(float AP, float RP, float HP, float V){
  int loadAPChange = detectActivePowerStep(AP, V);
  int loadRPChange = detectReactivePowerStep(RP, V);
  int loadHPChange = detectHarmonicPowerStep(HP, V);
  int loadIndex = 0;                                            // Index of the load that needs to change state
  if (fabsf(AP) < 100){                                          // If active power is close to zero --> set all loads to 'off'.
    for (int i = 0; i < 10; i++){ loadOn[i]=0; }
  }
  else if (loadAPChange){ //&& ((loadAPChange == loadRPChange) || (loadAPChange == loadHPChange))){  DANI (checks if active, reactive and harmonic power are correct)
    loadIndex = (int) fabsf(loadAPChange) - 1;                  // Converts into array index
    if ( loadAPChange > 0 ) { loadOn[loadIndex] = 1; }          // Load is turned on
    else if ( loadAPChange < 0 ) { loadOn[loadIndex] = 0; }     // Load is turned off
  }
}

//////////////////////////// Old functions //////////////////////////////////////

//int detectLoads(float activePower, float voltage){      // Returns: No loads: 0, load1: 1, load2: 2, both loads: 3.
//  int load = 0;                                         // returns 0 if no loads are detected
//  if (loadOn(3, activePower, voltage)){ load = 3;}      // Both loads are on
//  else if (loadOn(1, activePower, voltage)) {           // Power is larger than load 1
//    if (loadOn(2, activePower, voltage)){               // power is larger than both loads
//      if (load1Consumption < load2Consumption) {load = 2;}
//      else {load = 1;}                                  // --> If both loads have equal consumption, only load1 will display       
//    } 
//    else {load = 1;}
//  }
//  else if (loadOn(2, activePower, voltage)) {load = 2;}
//  return load;
//}
//
//int loadOn(int loadNum, float activePower, float voltage){   // Set loadNum to 3 for both loads
//  int on = 0;
//  float loadConsumption = 0;
//  switch (loadNum){
//  case (1) : loadConsumption = load1Consumption ; break ;
//  case (2) : loadConsumption = load2Consumption; break ;
//  case (3) : loadConsumption = load1Consumption + load2Consumption;  break ;     // Both loads
//  default : break ;
//  } 
//  on = ( (voltageCompensation(voltage, activePower)) > (loadConsumption[loadNum-1])*(1 - threshhold / 100.0) ) ;
//  return on;
//}

/*
Comments:
- All functions beeing used outside of the class use the load number, hence 1-10. Within the class the index is used, 0-9.
- When detecting the steps, the device needs to be turned on, while the program is running in order for it to be detected. 
- Two loads should never be plugged in the same read interval. In than case non of the will be detected (out of threshhold)
*/