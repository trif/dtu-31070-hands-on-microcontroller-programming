/*************************************************************************
*
*    Used with ICCARM and AARM.
*
*    (c) Copyright IAR Systems 2008
*
*    File name   : main.c
*    Description : Main module
*
*    History :
*    1. Date        : August 5, 2008
*       Author      : Stanimir Bonev
*       Description : Create
*
*  This example project shows how to use the IAR Embedded Workbench for ARM
* to develop code for the IAR LPC2478-SK board.
*  It implements USB CDC (Communication Device Class) device and install
* it like a Virtual COM port. The UART0 is used for physical implementation
* of the RS232 port.
*
* Jumpers:
*  EXT/JLINK  - depending of power source
*  ISP_E      - unfilled
*  RST_E      - unfilled
*  BDS_E      - unfilled
*  C/SC       - SC
*
* Note:
*  After power-up the controller gets it's clock from internal RC oscillator that
* is unstable and may fail with J-Link auto detect, therefore adaptive clocking
* should always be used. The adaptive clock can be select from menu:
*  Project->Options..., section Debugger->J-Link/J-Trace  JTAG Speed - Adaptive.
*
* The LCD shares pins with Trace port. If ETM is enabled the LCD will not work.
*
*    $Revision: 47021 $
**************************************************************************/
#include "includes.h"
#include <math.h>

#define NONPROT 0xFFFFFFFF
#define CRP1  	0x12345678
#define CRP2  	0x87654321
/*If CRP3 is selected, no future factory testing can be performed on the device*/
#define CRP3  	0x43218765

#ifndef SDRAM_DEBUG
#pragma segment=".crp"
#pragma location=".crp"
__root const unsigned crp = NONPROT;
#endif

#define LCD_VRAM_BASE_ADDR ((Int32U)&SDRAM_BASE_ADDR)

#define NUMBER_ITERATIONS 1  //defines how the average is made to determine Voltage, Current, Active Power...

extern Int32U SDRAM_BASE_ADDR;
extern FontType_t Terminal_6_8_6;
extern FontType_t Terminal_9_12_6;
extern FontType_t Terminal_18_24_12;

//implemented functions
int ASCIIHexToInt(Int8U c);
int hex2dec(char hex);
void printButtons() ;
float getVoltage(pInt8U pBuffer) ;
float getCurrent(pInt8U pBuffer) ;
float getAP(pInt8U pBuffer) ; //Active Power
float average(float *array) ; // gives the average of an array with a size of NUMBER_ITERATIONS
float getRP(pInt8U pBuffer) ; //Reactive Power
float getHP(pInt8U pBuffer) ; //Harmonic Power

#define BUF ((struct uip_eth_hdr *)&uip_buf[0])

/*************************************************************************
* Function Name: uip_log
* Parameters: none
*
* Return: none
*
* Description: Events logging
*
*************************************************************************/
void uip_log (char *m)
{
}

/*************************************************************************
* Function Name: main
* Parameters: none
*
* Return: none
*
* Description: main
*
*************************************************************************/
int main(void)
{
  Int32U cursor_x = (C_GLCD_H_SIZE - CURSOR_H_SIZE)/2, cursor_y = (C_GLCD_V_SIZE - CURSOR_V_SIZE)/2;
  ToushRes_t XY_Touch;
  Boolean Touch = FALSE;
  Boolean buff = FALSE, consumption = FALSE, refresh = FALSE, acquire = FALSE, loadsRemain = TRUE ; // to decide what we want to display
  Int8U Buffer[BUFFER_SIZE]; //BUFFER_SIZE is defined in uart.c
  Int32U iteration ;
  float Current = 0, AP =0, RP = 0, HP = 0, Volt = 0, Voltages[NUMBER_ITERATIONS]={0}, APs[NUMBER_ITERATIONS] ={0}, RPs[NUMBER_ITERATIONS] ={0}, HPs[NUMBER_ITERATIONS] ={0};
  int pointeur = 0, autorefresh = 0 ; // data for the webserver
  pInt32U pDst ; // to refresh the screen
  float noLoadAP = 0, noLoadRP = 0, noLoadHP = 0;
  unsigned int i;
  uip_ipaddr_t ipaddr;
  struct timer periodic_timer, arp_timer;
  int loadNumber = 0 ;
  
  int mode = 1 ;
  
  GLCD_Ctrl (FALSE);
  // Init GPIO
  GpioInit();
#ifndef SDRAM_DEBUG
  // MAM init
  MAMCR_bit.MODECTRL = 0;
  MAMTIM_bit.CYCLES  = 3;   // FCLK > 40 MHz
  MAMCR_bit.MODECTRL = 2;   // MAM functions fully enabled
  // Init clock
  InitClock();
  // SDRAM Init
  SDRAM_Init();
#endif // SDRAM_DEBUG
  // Init VIC
  VIC_Init();
  // GLCD init
  GLCD_Init (wallpaperPic.pPicStream, NULL);
  // Disable Hardware cursor
  GLCD_Cursor_Dis(0);
  // Init UART 0
  UART_init(UART_0,4,NORM);
  
  __enable_interrupt();
  
  //Cursor Initialization
  GLCD_Cursor_Dis(0);
  GLCD_Copy_Cursor ((Int32U *)Cursor, 0, sizeof(Cursor)/sizeof(Int32U));
  GLCD_Cursor_Cfg(CRSR_FRAME_SYNC | CRSR_PIX_32);
  GLCD_Move_Cursor(cursor_x, cursor_y);
  GLCD_Cursor_En(0);
  
  // Sys timer init 1/100 sec tick
  clock_init(2);
  
  timer_set(&periodic_timer, CLOCK_SECOND / 2);
  timer_set(&arp_timer, CLOCK_SECOND * 10);
  
  // Init touch screen
  TouchScrInit();
  
  // Touched indication LED
  USB_H_LINK_LED_SEL = 0; // GPIO
  USB_H_LINK_LED_FSET = USB_H_LINK_LED_MASK;
  USB_H_LINK_LED_FDIR |= USB_H_LINK_LED_MASK;
  
  // Enable GLCD
  GLCD_Ctrl (TRUE);
  
  //NILM initialize
  NILM_Init(10.0, 230.0);        // Set threshhold and Vnom
  
  // Initialize the ethernet device driver
  do
  {
    GLCD_TextSetPos(0,0);
  }
  while(!tapdev_init()); // add &&0 if ethernet cable is not connected
  GLCD_TextSetPos(0,0);
  
  
  // uIP web server
  // Initialize the uIP TCP/IP stack.
  uip_init();
  
  uip_ipaddr(ipaddr, 192,168,0,100);
  uip_sethostaddr(ipaddr);
  uip_ipaddr(ipaddr, 192,168,0,1);
  uip_setdraddr(ipaddr);
  uip_ipaddr(ipaddr, 255,255,255,0);
  uip_setnetmask(ipaddr);
  
  // Initialize the HTTP server.
  httpd_init();
  
  //Initialization of Buffer
  for(int k=0;k<BUFFER_SIZE;k++){
    Buffer[k]=0 ;
  }
  
  
  /*
  ----------------------------------------------------------------------- Infinite loop start -----------------------------------------------------------------------
  */
  while(1)
  {
    //webserver
    uip_len = tapdev_read(uip_buf);
    if(uip_len > 0)
    {
      if(BUF->type == htons(UIP_ETHTYPE_IP))
      {
        uip_arp_ipin();
        uip_input();
        /* If the above function invocation resulted in data that
        should be sent out on the network, the global variable
        uip_len is set to a value > 0. */
        if(uip_len > 0)
        {
          uip_arp_out();
          tapdev_send(uip_buf,uip_len);
        }
      }
      else if(BUF->type == htons(UIP_ETHTYPE_ARP))
      {
        uip_arp_arpin();
        /* If the above function invocation resulted in data that
        should be sent out on the network, the global variable
        uip_len is set to a value > 0. */
        if(uip_len > 0)
        {
          tapdev_send(uip_buf,uip_len);
        }
      }
    }
    else if(timer_expired(&periodic_timer))
    {
      timer_reset(&periodic_timer);
      for(i = 0; i < UIP_CONNS; i++)
      {
        uip_periodic(i);
        /* If the above function invocation resulted in data that
        should be sent out on the network, the global variable
        uip_len is set to a value > 0. */
        if(uip_len > 0)
        {
          uip_arp_out();
          tapdev_send(uip_buf,uip_len);
        }
      }
#if UIP_UDP
      for(i = 0; i < UIP_UDP_CONNS; i++) {
        uip_udp_periodic(i);
        /* If the above function invocation resulted in data that
        should be sent out on the network, the global variable
        uip_len is set to a value > 0. */
        if(uip_len > 0) {
          uip_arp_out();
          tapdev_send();
        }
      }
#endif /* UIP_UDP */
      /* Call the ARP timer function every 10 seconds. */
      if(timer_expired(&arp_timer))
      {
        timer_reset(&arp_timer);
        uip_arp_timer();
      }
    }
    
    if(mode == 0){
      //learning mode
      loadNumber = 0 ;
      while(loadNumber<11 && loadsRemain){
        pDst = (pInt32U) LCD_VRAM_BASE_ADDR;
        
        if(loadNumber){
          GLCD_SetFont(&Terminal_18_24_12,0x00e7f00d,0x00000000);
          GLCD_SetWindow(60,100,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("Please plug load %d", loadNumber);
          
          GLCD_SetFont(&Terminal_9_12_6,0x00000000,0x00afafaf);
          GLCD_SetWindow(70,160,260,220);
          GLCD_TextSetPos(0,0);
          GLCD_print("\r\n  Press when load %d is plugged  \r\n                                 ", loadNumber);
          
          GLCD_SetFont(&Terminal_9_12_6,0x006600ff,0x00606060);
          GLCD_SetWindow(215,10,310,70);
          GLCD_TextSetPos(0,0);
          GLCD_print("\r\n  No more loads  \r\n                 ");
        }
        else{   // No loads connected
          GLCD_SetFont(&Terminal_18_24_12,0x00e7f00d,0x00000000);
          GLCD_SetWindow(20,100,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("Please unplug all loads");
          
          GLCD_SetFont(&Terminal_9_12_6,0x00000000,0x00afafaf);
          GLCD_SetWindow(60,160,275,220);
          GLCD_TextSetPos(0,0);
          GLCD_print("\r\n Press when all loads are unplugged \r\n                                      ");
        }
        
        //enable touch
        if(TouchGet(&XY_Touch))
        {
          cursor_x = XY_Touch.X;
          cursor_y = XY_Touch.Y;
          GLCD_Move_Cursor(cursor_x, cursor_y);
          if (FALSE == Touch)
          {
            Touch = TRUE;
            USB_H_LINK_LED_FCLR = USB_H_LINK_LED_MASK;
          }
          if(cursor_x<300 && cursor_x>10 && cursor_y<230 && cursor_y>150){
            acquire = TRUE ;
          }
          if(loadNumber && cursor_x<320 && cursor_x>220 && cursor_y<80 && cursor_y>0){loadsRemain = FALSE ;} // get out of the while loop (no more loads)
        }
        else if(Touch)
        {
          USB_H_LINK_LED_FSET = USB_H_LINK_LED_MASK;
          Touch = FALSE;
        }
        
        if(acquire){
          iteration = 0 ;
          do{
            if(loadNumber){
              UART_Check(Buffer); //renew the buffer
            }
            
            // Data from UART1
            UART_Check(Buffer);
            
            if(Buffer[0]!='E'){ 
              APs[iteration] = getAP(Buffer) ;
              RPs[iteration] = getRP(Buffer) ;
              HPs[iteration] = getHP(Buffer) ;
              if (!loadNumber){     // No load connected
                Voltages[iteration]= getVoltage(Buffer) ; 
              }     
              iteration++ ;
            }
          } while(iteration<NUMBER_ITERATIONS) ;
          if(loadNumber)
          { // Set consumptions of loads
            setConsumption(loadNumber,(float) (average(APs) - noLoadAP) , (float) average(Voltages) );
            setConsumptionReactive(loadNumber,(float) (average(RPs) - noLoadRP) , (float) average(Voltages) );
            setConsumptionHarmonic(loadNumber,(float) (average(HPs) - noLoadHP) , (float) average(Voltages) );
          }    
          else{                                                                 // Set no load                
            noLoadAP = average(APs);
            noLoadRP = average(RPs);
            noLoadHP = average(HPs);
            updateLoads((float) noLoadAP, (float) noLoadRP, (float) noLoadHP, (float) average(Voltages));
          }                             // Set nominal voltage        
          loadNumber++ ;
          refresh = TRUE ;
          acquire = FALSE ;
        }
        
        if(refresh){
          for(Int32U i = 0; (C_GLCD_H_SIZE * C_GLCD_V_SIZE) > i; i++)
          {
            pDst[i] = wallpaperPic.pPicStream[i] ;
          }
          refresh = FALSE ;
        }
        
      }
      loadNumber-- ; // to get the right number of loads
      mode = 1 ;
      loadsRemain = TRUE ;
      for(Int32U i = 0; (C_GLCD_H_SIZE * C_GLCD_V_SIZE) > i; i++) //refresh
      {
        pDst[i] = wallpaperPic.pPicStream[i] ;
      }
    }
    
    //monitoring mode
    if(mode == 1){
      printButtons() ;
      GLCD_SetFont(&Terminal_9_12_6,0x0000FF,0x000cd4ff);
      pDst = (pInt32U) LCD_VRAM_BASE_ADDR;
      
      // Data from UART1
      UART_Check(Buffer) ;
      
      
      //enable touch
      if(TouchGet(&XY_Touch))
      {
        cursor_x = XY_Touch.X;
        cursor_y = XY_Touch.Y;
        GLCD_Move_Cursor(cursor_x, cursor_y);
        if (FALSE == Touch)
        {
          Touch = TRUE;
          USB_H_LINK_LED_FCLR = USB_H_LINK_LED_MASK;
        }
        if(cursor_x<95 && 20 <cursor_x && cursor_y <230 && 190<cursor_y){buff = TRUE ; consumption = FALSE ; refresh = TRUE ;}
        else if(cursor_x<195 && 120<cursor_x && cursor_y <230 && 190<cursor_y){buff = FALSE ; consumption = TRUE ; refresh = TRUE;}
        else if(cursor_x<295 && 220<cursor_x && cursor_y <230 && 190<cursor_y){buff = FALSE; consumption = FALSE ; refresh = TRUE ;}
        
        if(cursor_x<300 && 220<cursor_x && cursor_y <50 && 10<cursor_y){mode = 0; refresh = TRUE ;}
        
        if(refresh){
          for(Int32U i = 0; (C_GLCD_H_SIZE * C_GLCD_V_SIZE) > i; i++)
          {
            pDst[i] = wallpaperPic.pPicStream[i] ;
          }
          if(mode == 1){
            printButtons() ;
          }
          
          refresh = FALSE ;
        }
      }
      else if(Touch)
      {
        USB_H_LINK_LED_FSET = USB_H_LINK_LED_MASK;
        Touch = FALSE;
      }
      
      
      
      if(Buffer[0]!='E')
      {   
        //autorefresh every ten seconds
        if(refresh>=10){
          for(Int32U i = 0; (C_GLCD_H_SIZE * C_GLCD_V_SIZE) > i; i++)
          {
            pDst[i] = wallpaperPic.pPicStream[i] ;
          }
          refresh = 0 ;
        }
        else{
          refresh++ ;
        }
        
        if(buff) {
          GLCD_SetFont(&Terminal_9_12_6,0x0000FF,0x000cd4ff);
          
          //hexadecimal
          GLCD_SetWindow(10,60,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("Hexadecimal content : "); 
          
          GLCD_SetWindow(10,80,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("%s", Buffer);
          
          //decimal
          GLCD_SetWindow(10,100,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("Decimal content : "); 
          
          for(int compteur=0;compteur<BUFFER_SIZE;compteur++){
            GLCD_SetWindow(10+25*(compteur%12),120 + 25*(compteur/12),310,211);
            GLCD_TextSetPos(0,0);
            if(Buffer[compteur]<10){
              GLCD_print("00%d  ", Buffer[compteur]);
            }
            else if(Buffer[compteur]<100){
              GLCD_print("0%d  ", Buffer[compteur]);
            }
            else {GLCD_print("%d  ", Buffer[compteur]);}
          }
          
        }
        else if (consumption) {
          GLCD_SetFont(&Terminal_18_24_12,0x000f7230c,0x00000000);
          
          //Voltage
          GLCD_SetWindow(90,100,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("V = %d V", (int) Volt) ;       
          
          //Current
          GLCD_SetWindow(90,140,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("I = %d mA", (int) Current) ;
          
          //Total Power
          GLCD_SetWindow(50,60,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("Total power = %d W    ", (int) (0.001*Current*Volt)) ;
        }
        else{
          Volt = getVoltage(Buffer) ; Current = getCurrent(Buffer) ; AP = getAP(Buffer) ;
          
          fillStats(pointeur, (int) (0.001*Volt*Current), (int) Volt, (int) Current) ;
          if(pointeur<10){
            pointeur++ ;
          }
          else{
            pointeur = 0 ;
          }
          
          GLCD_SetFont(&Terminal_9_12_6,0x000f7230c,0x00000000);
          
          //Voltage
          GLCD_SetWindow(230,150,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("V = %d V", (int) Volt) ;
          
          //Reactive and harmonic powers
          GLCD_SetWindow(100,130,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("RP = %d, HP = %d", (int) getRP(Buffer), (int) getHP(Buffer)) ;
          
          //Current
          GLCD_SetWindow(230,170,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("I = %d mA", (int) Current) ;
          
          //Total Power
          GLCD_SetFont(&Terminal_18_24_12,0x000f7230c,0x00000000);
          GLCD_SetWindow(5,15,310,211);
          GLCD_TextSetPos(0,0);
          GLCD_print("Power = %d W    ", (int) (0.001*Current*Volt)) ;
          
          
          updateLoads(AP, RP, HP, Volt);
          
          //States of loads
          for(int l = 1 ; l<= loadNumber;l++){
            GLCD_SetFont(&Terminal_9_12_6,0x00ffffff,0x000000000);
            GLCD_SetWindow(5+60*((l-1)%5),55 + (l/6)*45 ,310,211);
            GLCD_TextSetPos(0,0);
            GLCD_print("load %d",l);
            if(getLoadOn(l)){
              GLCD_SetFont(&Terminal_9_12_6,0x0034c924,0x000000000);
              GLCD_SetWindow(5+60*((l-1)%5),67+ (l/6)*45,310,211);
              GLCD_TextSetPos(0,0);
              GLCD_print("on    ");
              
              GLCD_SetFont(&Terminal_9_12_6,0x000cd4ff,0x000000000);
              GLCD_SetWindow(5+60*((l-1)%5),79+ (l/6)*45,310,211);
              GLCD_TextSetPos(0,0);
              GLCD_print(" %d W  ", (int)(0.001*Current*Volt*(getLoadConsumption(l)/AP)));
            }
            else{
              GLCD_SetFont(&Terminal_9_12_6,0x0000FF,0x000000000);
              GLCD_SetWindow(5+60*((l-1)%5),67+ (l/6)*45,310,211);
              GLCD_TextSetPos(0,0);
              GLCD_print("off   ");
              
              GLCD_SetFont(&Terminal_9_12_6,0x000cd4ff,0x000000000);
              GLCD_SetWindow(5+60*((l-1)%5),79+ (l/6)*45,310,211);
              GLCD_TextSetPos(0,0);
              GLCD_print(" 0 W  ");
            }
          }
          
          
          
          //          GLCD_SetWindow(100,32,310,211);
          //          GLCD_TextSetPos(0,0);
          //          GLCD_print("Detect AP step: %d", (int) detectActivePowerStep(AP, Volt) ); 
          
          //          GLCD_SetWindow(10,45,310,211);
          //          GLCD_TextSetPos(0,0);
          //          GLCD_print("load consumption : ");
          //          GLCD_print("%d and %d", (int) getLoadConsumption(1), (int) getLoadConsumption(2));
          //          
          //          GLCD_SetWindow(10,60,310,211);
          //          GLCD_TextSetPos(0,0);
          //          GLCD_print("APs : ");
          //          for(int j=0;j<NUMBER_ITERATIONS;j++){
          //            GLCD_print("%d ", (int) APs[j]);}
          //          
          //          GLCD_SetWindow(10,75,310,211);
          //          GLCD_TextSetPos(0,0);
          //          GLCD_print("previousAP : ");
          //          GLCD_print("%d ", (int) getPreviousAP());
          
          
        }   
      }  
    }
    
  }
  /*
  ----------------------------------------------------------------------- Infinite loop end -----------------------------------------------------------------------
  */
}

// Converts a hex diget represented in an 8bit ASCII format into the corresponding decimal number
int ASCIIHexToInt(Int8U c){  
  Int8U num = 0;
  if ((c > 47) & (c < 58)) num = c - 48;                // Numbers 0-9
  else if ((c > 96) & (c < 103)) num = c - 86;          // Lowercase letters a -f
  else if ((c > 64) & (c < 71)) num = c - 54;           // Uppercase letters A-F    
  return num;
}

int hex2dec(char hex){
  switch(hex){
  case 48 : return 0 ;
  case 49 : return 1 ;
  case 50 : return 2 ;
  case 51 : return 3 ;
  case 52 : return 4 ;
  case 53 : return 5 ;
  case 54 : return 6 ;
  case 55 : return 7 ;
  case 56 : return 8 ;
  case 57 : return 9 ;
  case (97): return 10 ;
  case (98) : return 11 ;
  case (99) : return 12 ;
  case (100) : return 13 ;
  case (101) : return 14 ;
  case (102) : return 15 ;
  default : return 0 ;
  }
}

void printButtons(){
  GLCD_SetFont(&Terminal_9_12_6,0x000000,0x0009fe855) ;
  GLCD_SetWindow(20,190,95,230);
  GLCD_TextSetPos(0,0);
  GLCD_print("\r\n    Buffer    \r\n              ");
  
  GLCD_SetFont(&Terminal_9_12_6,0x000000,0x0006600ff) ;
  GLCD_SetWindow(120,190,195,230);
  GLCD_TextSetPos(0,0);
  GLCD_print("\r\n Consumption \r\n              ");
  
  GLCD_SetFont(&Terminal_9_12_6,0x000000,0x000f7230c) ;
  GLCD_SetWindow(220,190,300,230);
  GLCD_TextSetPos(0,0);
  GLCD_print("\r\n  Monitoring  \r\n                ");
  
  GLCD_SetFont(&Terminal_9_12_6,0x000000,0x000ffffff) ;
  GLCD_SetWindow(215,10,300,50);
  GLCD_TextSetPos(0,0);
  GLCD_print("\r\n Learning mode \r\n                ");
}

float getVoltage(pInt8U pBuffer){
  int Volt = 0 ;
  for(int compteur=1;compteur<7;compteur++){    
    Volt += pow(16,5-(compteur-1))*hex2dec(pBuffer[compteur]) ;
  }
  return ((float) Volt)/48796.0+21.9 ;  // Converts to SI-units (V)
}

float getCurrent(pInt8U pBuffer){
  int Current = 0 ;
  for(int compteur=7;compteur<13;compteur++){    
    Current += pow(16,5-(compteur-7))*hex2dec(pBuffer[compteur]) ;
  }
  return ((float) Current)*0.0016375 ;
}

float getAP(pInt8U pBuffer){
  signed int AP =0 ;
  for(int compteur=13;compteur<19;compteur++){    
    AP += pow(16,5-(compteur-13))*hex2dec(pBuffer[compteur]) ;
  }
  if(AP < 8*pow(16,5)){
    return (float) -AP ;}
  else{return (float) (pow(16,6)-AP);}
}

float getRP(pInt8U pBuffer){   //Reactive Power
  signed int RP =0 ;
  for(int compteur=19;compteur<25;compteur++){    
    RP += pow(16,5-(compteur-19))*hex2dec(pBuffer[compteur]) ;
  }
  if(RP < 8*pow(16,5)){
    return (float) -RP ;}
  else{return (float) (pow(16,6)-RP);}
}

float getHP(pInt8U pBuffer){   //Harmonic Power
  signed int HP =0 ;
  for(int compteur=25;compteur<31;compteur++){    
    HP += pow(16,5-(compteur-25))*hex2dec(pBuffer[compteur]) ;
  }
  if(HP < 8*pow(16,5)){
    return (float) -HP ;}
  else{return (float) (pow(16,6)-HP);}
}

float average(float *array){
  float sum = 0 ;
  for(int k=0;k<NUMBER_ITERATIONS;k++){
    sum+=array[k] ;
  }
  return sum / ((float) NUMBER_ITERATIONS) ;
}